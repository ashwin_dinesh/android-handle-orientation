package com.ashwin.example.question13;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private boolean mIsRunning;
    private EditText mEditText1, mEditText2, mEditText3, mEditText4, mEditText5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        mEditText1 = (EditText) findViewById(R.id.editText1);
        mEditText1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mIsRunning) {
                                // Clear focus here from edittext
                                mEditText1.clearFocus();
                            }
                        }
                    }, 500);
                }
                return false;
            }
        });

        mEditText2 = (EditText) findViewById(R.id.editText2);
        mEditText2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(mIsRunning) {
                                // Clear focus here from edittext
                                mEditText2.clearFocus();
                            }
                        }
                    }, 500);
                }
                return false;
            }
        });

        mEditText3 = (EditText) findViewById(R.id.editText3);
        mEditText3.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mIsRunning) {
                                // Clear focus here from edittext
                                mEditText3.clearFocus();
                            }
                        }
                    }, 500);
                }
                return false;
            }
        });

        mEditText4 = (EditText) findViewById(R.id.editText4);
        mEditText4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mIsRunning) {
                                // Clear focus here from edittext
                                mEditText4.clearFocus();
                            }
                        }
                    }, 500);
                }
                return false;
            }
        });

        mEditText5 = (EditText) findViewById(R.id.editText5);
        mEditText5.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mIsRunning) {
                                // Clear focus here from edittext
                                mEditText5.clearFocus();
                            }
                        }
                    }, 500);
                }
                return false;
            }
        });

        // Setting dummy editable values
        mEditText1.setText("Edit text 1");
        mEditText2.setText("Edit text 2");
        mEditText3.setText("Edit text 3");
        mEditText4.setText("Edit text 4");
        mEditText5.setText("Edit text 5");
    }

    @Override
    protected void onResume() {
        mIsRunning = true;
        super.onResume();
    }

    @Override
    protected void onPause() {
        mIsRunning = false;
        super.onPause();
    }
}
